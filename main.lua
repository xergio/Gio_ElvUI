
local E, L, V, P, G = unpack(ElvUI)
local _G = _G
local UF = E:GetModule("UnitFrames")
local hooksecurefunc = hooksecurefunc
local MAX_COMBO_POINTS = MAX_COMBO_POINTS

local color = {
   r = 1,
   g = 1,
   b = 1
}

local bgColor = {
   r = 0.1,
   g = 0.1,
   b = 0.1
}

-- PowerBar & AdditionalPowerBar
local function hook_PostUpdatePower(self)
   self:SetStatusBarColor(color.r, color.g, color.b)
   self.bg:SetVertexColor(bgColor.r, bgColor.g, bgColor.b)
end

local function hook_PostUpdateAdditionalPower(self)
   self:Hide()
end

local f = CreateFrame("Frame")
f:RegisterEvent("PLAYER_ENTERING_WORLD")
f:SetScript("OnEvent", function(self)
      self:UnregisterEvent("PLAYER_ENTERING_WORLD")
      
      local unitframe = _G["ElvUF_Player"]
      
      local power = unitframe and unitframe.Power
      if power then
         hooksecurefunc(power, "PostUpdate", hook_PostUpdatePower)
      end

      local apower = unitframe and unitframe.AdditionalPower
      if apower then
         hooksecurefunc(apower, "PostUpdate", hook_PostUpdateAdditionalPower)
      end
end)

-- ClassBar
local function hook_Configure_ClassBar(self, frame)
   local bars = frame[frame.ClassBar]
   if not bars then return end
   
   if (frame.ClassBar == 'ClassPower' or frame.ClassBar == 'Runes') then
      local maxClassBarButtons = max(UF.classMaxResourceBar[E.myclass] or 0, MAX_COMBO_POINTS)
      for i = 1, maxClassBarButtons do
         if i <= frame.MAX_CLASS_BAR and bars[i].bg then
            bars[i]:SetStatusBarColor(color.r, color.g, color.b)
            bars[i].bg:SetVertexColor(bgColor.r, bgColor.g, bgColor.b)
         end
      end
   end
end
hooksecurefunc(UF, "Configure_ClassBar", hook_Configure_ClassBar)
